/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication7;

import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.*;


public class GrafoGil {
     //////////Variables de instancia/////////
  private Vertice[] nodes;
  private HashMap<Vertice, HashSet<Vertice>> graph;
  private final int numeroVertices; //número de vértices del grafo
  private int numeroAristas;  //número de aristas únicas del grafo
  private static Formatter output; //objeto para escribir a disco


  //////////Constructores//////////

  public GrafoGil(int numVertices) {
    this.graph = new HashMap<Vertice, HashSet<Vertice>>();
    this.numeroVertices = numVertices;
    this.nodes = new Vertice[numVertices];
    for (int i = 0; i < numVertices; i++) {
      Vertice n = new Vertice(i);
      this.nodes[i] = n;
      this.graph.put(n, new HashSet<Vertice>());
    }
  }
  
  public int gradoVertice(int i) {
    Vertice n1 = this.getNode(i);
    return this.graph.get(n1).size();
  }

  public void conectarVertices(int i, int j,boolean dirigido) {
    /*Se recuperan los vértices de los índices i y j*/
     Vertice n1 = this.getNode(i);
     Vertice n2 = this.getNode(j);
     /*Se recuperan las aristas de cada vértice*/
     HashSet<Vertice> aristas1 = this.getEdges(i);
     HashSet<Vertice> aristas2 = this.getEdges(j);

     /*Se agregan los vértices al conjunto del otro*/
     if(dirigido==true){
         aristas1.add(n2);
         this.numeroAristas=numeroAristas+1;
     }
     else{
     aristas1.add(n2);
     aristas2.add(n1);  
     this.numeroAristas +=1;
     }
  }

  //Regresa 'true' si ya existe la arista
  private Boolean existeConexion(int i, int j) {
    /*Se recuperan los vértices de los índices i y j*/
     Vertice n1 = this.getNode(i);
     Vertice n2 = this.getNode(j);
    /*Se recuperan las aristas de cada vértice*/
    HashSet<Vertice> aristas1 = this.getEdges(i);
    HashSet<Vertice> aristas2 = this.getEdges(j);
    /*Se revisa que un nodo esté en el conjunto de aristas del otro*/
     if (aristas1.contains(n2) || aristas2.contains(n1)) {
       return true;
     }
     else{
       return false;
     }
  }

  //////////getters/setters de las variables de instancia//////////
  public int getNumNodes() {return numeroVertices;}

  public int getNumEdges() {return numeroAristas;}

  public Vertice getNode(int i) {return this.nodes[i];}


  public HashSet<Vertice> getEdges(int i) {
    Vertice n = this.getNode(i);
    return this.graph.get(n);
  }


  //////////Método toString para representación en String del GrafoER//////////
  public String toString() {
    String salida;
      salida ="graph {\n";
      for (int i = 0; i < this.getNumNodes(); i++) {
        salida += this.getNode(i).getName() + ";\n";
      }
      for (int i = 0; i < this.getNumNodes(); i++) {
        HashSet<Vertice> aristas = this.getEdges(i);
        for (Vertice n : aristas) {
        salida += this.getNode(i).getName() + " -- " + n.getName() + ";\n";
        }
       }
      salida += "}\n";
    return salida;
  }
  
  public void modeloGilbert(double probabilidad, boolean dirigido, boolean autociclo) {
    Random randomNum = new Random();
    if ((dirigido==true)& (autociclo==true)){
    for(int i = 0; i < this.getNumNodes(); i++) {
        for(int j = 0; j <this.getNumNodes(); j++) {
            if ((i != j) && (randomNum.nextDouble() <= probabilidad)) {
                if (!existeConexion(i, j)) {
                    conectarVertices(i, j,true);
                }
             }   
            else if ((i == j) && (randomNum.nextDouble() <= probabilidad)){
                if (!existeConexion(i, j)) {
                    conectarVertices(i, j,true);
                }
            }
        }
    }
    }
    if ((dirigido==true)& (autociclo==false)){
    for(int i = 0; i < this.getNumNodes(); i++) {
        for(int j = 0; j <this.getNumNodes(); j++) {
            if ((i != j) && (randomNum.nextDouble() <= probabilidad)) {
                if (!existeConexion(i, j)) {
                    conectarVertices(i, j,true);
                }
             }   
        }
    }
    }
    if ((dirigido==false)& (autociclo==false)){
    for(int i = 0; i < this.getNumNodes(); i++) {
        for(int j = 0; j <this.getNumNodes(); j++) {
            if ((i != j) && (randomNum.nextDouble() <= probabilidad)) {
                if (!existeConexion(i, j)) {
                    conectarVertices(i, j,false);
                }
             }   
        }
    }
    }
    if ((dirigido==false)& (autociclo==true)){
    for(int i = 0; i < this.getNumNodes(); i++) {
        for(int j = 0; j <this.getNumNodes(); j++) {
            if ((i != j) && (randomNum.nextDouble() <= probabilidad)) {
                if (!existeConexion(i, j)) {
                    conectarVertices(i, j,false);
                }
             }   
            else if ((i == j) && (randomNum.nextDouble() <= probabilidad)){
                if (!existeConexion(i, j)) {
                    conectarVertices(i, j,false);
                }
            }
        }
    }
    }
  }
  
    public void escribirArchivo(String nombre) {
    try{
      output = new Formatter(nombre);
    }
    catch (SecurityException securityException) {
      System.err.println("No hay permiso de escritura.");
      System.exit(1);
    }
    catch (FileNotFoundException fileNotFoundException) {
      System.err.println("Error al abrir el archivo.");
      System.exit(1);
    }
    try{
      output.format("%s",this);
    }
    catch (FormatterClosedException formatterClosedException) {
      System.err.println("Error al escribir al archivo");
    }
    if (output != null)
    output.close();
  }

  ////// SEGUNDA ENTREGA //////////

  public GrafoGil BFS(int s) {
    GrafoGil arbol = new GrafoGil(this.getNumNodes());  // grafo de salida
    Boolean[] discovered = new Boolean[this.getNumNodes()];  // arreglo aux
    PriorityQueue<Integer> L = new PriorityQueue<Integer>();
    discovered[s] = true;  // se pone como descubierto el vértice raíz
    for (int i = 0; i < this.getNumNodes(); i++) {
      if (i != s) {   // el resto como no descubiertos
        discovered[i] = false;
      }
    }
    L.add(s);  // Se agrega a la cola de prioridad el nodo raíz
    while (L.peek() != null) {  // se revisa que no esté vacía la cola
      int u = L.poll();  // se extrae un elemento de la cola
      HashSet<Vertice> aristas = this.getEdges(u);  // aristas del nodo u
      for (Vertice n : aristas) {
        if(!discovered[n.getIndex()]) {
          // si no está descubierto, conectarlo, marcarlo como descubierto
          // y agregarlo a la cola.
          arbol.conectarVertices(u, n.getIndex(),false);
          discovered[n.getIndex()] = true;
          L.add(n.getIndex());
        }
      }
    }
    return arbol;
  }

  public GrafoGil DFS_R(int s) {
  GrafoGil arbol = new GrafoGil(this.getNumNodes());  // grafo de salida
  Boolean[] discovered = new Boolean[this.getNumNodes()];  // arreglo aux
  for (int i = 0; i < this.getNumNodes(); i++) {
    discovered[i] = false;  // se marcan todos como no decubiertos
  }
  recursivoDFS(s, discovered, arbol);
  return arbol;
}

  private void recursivoDFS(int u, Boolean[] discovered, GrafoGil arbol) {
  discovered[u] = true; 
  HashSet<Vertice> aristas = this.getEdges(u);
  for (Vertice n : aristas) {
      if (!discovered[n.getIndex()]) {
        arbol.conectarVertices(u, n.getIndex(),false);
        recursivoDFS(n.getIndex(), discovered, arbol);
        }
      }
    }

  public GrafoGil DFS_I(int s) {
  GrafoGil arbol = new GrafoGil(this.getNumNodes());  // grafo de salida
  Boolean[] explored = new Boolean[this.getNumNodes()];  // arreglo aux
  Stack<Integer> S = new Stack<Integer>(); //pila para los vértices
  Integer[] parent = new Integer[this.getNumNodes()]; //arreglo de padres
  for (int i = 0; i < this.getNumNodes(); i++) {
      explored[i] = false;  //se ponen todos los vértices como no explorados
    }
  S.push(s);  //se manda a la pila al nodo raíz
  while(!S.isEmpty()) {
    
    int u = S.pop();  // se extraen elementos de la pila
    if(!explored[u]) {
      explored[u] = true;  // si aún no estaban explorados se marcan como tal
      if(u != s) {
        arbol.conectarVertices(u, parent[u],false); //se conecta con su padre
      }
      HashSet<Vertice> aristas = this.getEdges(u);  // aristas del nodo u
      for (Vertice n : aristas) {
        S.push(n.getIndex());  // a la pila los vértices conectados con u
        parent[n.getIndex()] = u;  // se les marca como padre a u
        }
      }
    }
  return arbol;
  }
}

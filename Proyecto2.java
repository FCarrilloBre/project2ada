/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication7;

import java.util.Scanner;
import javaapplication7.GrafoER;

public class Proyecto2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int modelo = 0;
        int n,m;
        Boolean Autci;
        while (modelo != 5) {
            System.out.println("Elige el modelo que quieres:");
            System.out.println("1.- Erdös-Renyi");
            System.out.println("2.- Modelo de Gilbert");
            System.out.println("3.- Modelo geografico simple");
            System.out.println("4.- Variante del modelo Barabasi Albert");
            System.out.println("5.- Salir");
            modelo = sc.nextInt();
            switch (modelo) {
                case 1:
                    System.out.println("Introduce el nombre de tu archivo .gv");
                    sc.nextLine();
                    String name = sc.nextLine();
                    
                    System.out.println("Introduce el número de nodos:");
                    
                    n = sc.nextInt();
                    System.out.println("Introduce el número de parejas posibles:");
                    m = sc.nextInt();
                    System.out.println("Introduce true si quieres que haya auotciclo");
                    System.out.println("De lo contratrio introduce false");
                    Autci = sc.nextBoolean();
                    System.out.println("Introduce true si quieres que sea dirigido");
                    System.out.println("De lo contratrio introduce false");
                    boolean dirig = sc.nextBoolean();
                    System.out.println("introduce el numero del nodo que sera la raiz");
                    int root = sc.nextInt();
                    sc.nextLine();
                    System.out.println("Introduce el nombre de tu archivo de BFS .gv");
                    String name2 = sc.nextLine();
                    System.out.println("Introduce el nombre de tu archivo de DFS recursivo .gv");
                    String name3 =sc.nextLine();
                    System.out.println("Introduce el nombre de tu archivo de DFS .gv");
                    String name4 =sc.nextLine();
                    GrafoER uno=new GrafoER(n);
                    uno.modeloER(m,dirig,Autci);
                    uno.escribirArchivo(name);
                    GrafoER unoBFS = uno.BFS(root);
                    unoBFS.escribirArchivo(name2);
                    GrafoER unoDFS_R = uno.DFS_R(root);
                    unoDFS_R.escribirArchivo(name3);
                    GrafoER unoDFS_I = uno.DFS_I(root);
                    unoDFS_I.escribirArchivo(name4);
                    break;
                case 2:
                    System.out.println("Introduce el nombre de tu archivo .gv");
                    sc.nextLine();
                    name = sc.nextLine();
                    
                    System.out.println("Introduce el número de nodos:");
                    
                    n = sc.nextInt();
                    System.out.println("Introduce la probabilidad de que esten conectados");
                    double p = sc.nextDouble();
                    System.out.println("Introduce true si quieres que haya auotciclo");
                    System.out.println("De lo contratrio introduce false");
                    Autci = sc.nextBoolean();
                    System.out.println("Introduce true si quieres que sea dirigido");
                    System.out.println("De lo contratrio introduce false");
                    dirig = sc.nextBoolean();
                    System.out.println("introduce el numero del nodo que sera la raiz");
                    root = sc.nextInt();
                    sc.nextLine();
                    System.out.println("Introduce el nombre de tu archivo de BFS .gv");
                    name2 = sc.nextLine();
                    System.out.println("Introduce el nombre de tu archivo de DFS recursivo .gv");
                    name3 =sc.nextLine();
                    System.out.println("Introduce el nombre de tu archivo de DFS .gv");
                    name4 =sc.nextLine();
                    GrafoGil dos=new GrafoGil(n);
                    dos.modeloGilbert(p,dirig,Autci);
                    dos.escribirArchivo(name);
                    GrafoGil dosBFS = dos.BFS(root);
                    dosBFS.escribirArchivo(name2);
                    GrafoGil dosDFS_R = dos.DFS_R(root);
                    dosDFS_R.escribirArchivo(name3);
                    GrafoGil dosDFS_I = dos.DFS_I(root);
                    dosDFS_I.escribirArchivo(name4);
                    break;
                case 3:
                    System.out.println("Introduce el nombre de tu archivo .gv");
                    sc.nextLine();
                    name = sc.nextLine();
                    
                    System.out.println("Introduce el número de nodos:");
                    n = sc.nextInt();
                    System.out.println("Introduce la distancia:");
                    double r = sc.nextDouble();
                    System.out.println("Introduce true si quieres que haya auotciclo");
                    System.out.println("De lo contratrio introduce false");
                    Autci = sc.nextBoolean();
                    System.out.println("Introduce true si quieres que sea dirigido");
                    System.out.println("De lo contratrio introduce false");
                    dirig = sc.nextBoolean();
                    System.out.println("introduce el numero del nodo que sera la raiz");
                    root = sc.nextInt();
                    sc.nextLine();
                    System.out.println("Introduce el nombre de tu archivo de BFS .gv");
                    name2 = sc.nextLine();
                    System.out.println("Introduce el nombre de tu archivo de DFS recursivo .gv");
                    name3 =sc.nextLine();
                    System.out.println("Introduce el nombre de tu archivo de DFS .gv");
                    name4 =sc.nextLine();
                    Grafogeo tres=new Grafogeo(n);
                    tres.modeloGeoSimple(r,dirig,Autci);
                    tres.escribirArchivo(name);
                    Grafogeo tresBFS = tres.BFS(root);
                    tresBFS.escribirArchivo(name2);
                    Grafogeo tresDFS_R = tres.DFS_R(root);
                    tresDFS_R.escribirArchivo(name3);
                    Grafogeo tresDFS_I = tres.DFS_I(root);
                    tresDFS_I.escribirArchivo(name4);
                    break;
                case 4:
                    System.out.println("Introduce el nombre de tu archivo .gv");
                    sc.nextLine();
                    name = sc.nextLine();
                    
                    System.out.println("Introduce el número de nodos:");
                    n = sc.nextInt();
                    System.out.println("Introduce el grado máximo por vertice:");
                    int grado = sc.nextInt();
                    System.out.println("Introduce true si quieres que sea dirigido");
                    System.out.println("De lo contratrio introduce false");
                    dirig = sc.nextBoolean();
                    System.out.println("introduce el numero del nodo que sera la raiz");
                    root = sc.nextInt();
                    sc.nextLine();
                    System.out.println("Introduce el nombre de tu archivo de BFS .gv");
                    name2 = sc.nextLine();
                    System.out.println("Introduce el nombre de tu archivo de DFS recursivo .gv");
                    name3 =sc.nextLine();
                    System.out.println("Introduce el nombre de tu archivo de DFS .gv");
                    name4 =sc.nextLine();
                    GrafoBA cuatro=new GrafoBA(n);
                    cuatro.modeloBA(grado,dirig);
                    cuatro.escribirArchivo(name);
                    GrafoBA cuatroBFS = cuatro.BFS(root);
                    cuatroBFS.escribirArchivo(name2);
                    GrafoBA cuatroDFS_R = cuatro.DFS_R(root);
                    cuatroDFS_R.escribirArchivo(name3);
                    GrafoBA cuatroDFS_I = cuatro.DFS_I(root);
                    cuatroDFS_I.escribirArchivo(name4);
                    break;
                default:
                    break;
    }
        }       
  }
}
